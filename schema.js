var mongoose = require('mongoose');
var googleSearchModel = void 0;
var initializeSearchModel = (context) => {
    mongoose.connect(context.MONGO_URL, {useMongoClient: true});
    var Schema = mongoose.Schema;
    var googleImageSearchHistory = new Schema({
        searchKey: "String",
        files: [{key: "String", name: "String"}]
    });

    googleSearchModel = mongoose.model("googleImageSearches", googleImageSearchHistory);
};

var getGoogleSearchModel = () => {
    return googleSearchModel;
}

module.exports = {
    initializeSearchModel,
    getGoogleSearchModel
}