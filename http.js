import {googleImages} from "./googleImages";
import {getGoogleSearchModel} from "./schema";
import {getDB, uploadFilesInMongo, downloadFile} from "./Mongo";

process.on('uncaughtException', function (err) {
    console.log("Uncaught exception comes at>>>>>" + date);
    console.log("err uncaught exception>>>>>>" + err + ">>>>stack>>>>" + err.stack);
});


var configure = (app, context) => {

    app.use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'access-control-allow-origin, content-type, accept');
        res.setHeader('Access-Control-Allow-Credentials', true);
        next();
    });

    app.options("*", (req, res) => {
        res.writeHead(200, {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "access-control-allow-origin, content-type, accept"
        });
        res.end();
    });

    app.all("/getData",(req,res)=>{
        setTimeout(()=>{
           res.send("hello i have received");
	},4000)
    })

    app.all("/upload", async (req, res) => {
        try {
            var requestParams = getRequestParams(req);
            uploadFilesOnServer(requestParams, context);
            writeJSONResponse({result: "Images are uploading once uploaded it will show in search history"}, req, res);
        } catch (err) {
            writeJSONResponse(err, req, res);
        }
    })

    app.all("/render/:fileKey", async (req, res) => {
        try {
            var requestParams = await  getRequestParams(req);
            var dbInstance = await getDB(context);
            var {data, fileName,contentType} = await  downloadFile(requestParams.fileKey, dbInstance);
            var options = {ignoreGzip: true, ignoreWrap: true};
            var download = requestParams.download;
            var inline = requestParams.inline || true;
            if (download || inline) {
                var head = {
                    "Cache-Control": "max-age=" + (86400000 * 7) + ",public",
                    "Connection": "keep-alive",
                    "Expires": new Date(Date.now() + (86400000 * 365)).toUTCString()
                };
                head["Content-Disposition"] = (download ? "attachment" : "inline") + "; filename=\"" + fileName + "\"";
                head["Content-Type"] = getContentType(fileName) || contentType;
                options["head"] = head;
            }
            writeJSONResponse(data, req, res, options);
        } catch (error) {
            writeJSONResponse(error, req, res);
        }
    })

    app.all("/getImageSearchData", async (req, res) => {
        try {
            var requestParams = await getRequestParams(req);

            var response = await getImageSearchData(requestParams, context);

            writeJSONResponse(response, req, res);
        } catch (err) {
            writeJSONResponse(err, req, res);
        }
    })

}


var writeJSONResponse = (result, req, resp, options = {}) => {
    var jsonResponseType = {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS"
    };
    if (result instanceof Error) {
        var responseToWrite = {
            status: "error"
        };
        var response = {
            stack: result.stack
        };

        response.message = result.message;
        response.code = result.code;
        responseToWrite.code = 500;
        responseToWrite.response = {
            error: response
        };
        try {
            resp.writeHead(responseToWrite.code, jsonResponseType);
        } catch (err) {
            console.log("err in write error>>>>>>>>>>>>>", err);
        }
        resp.write(JSON.stringify(responseToWrite));
        resp.end();
    } else {
        var customHeader = options.head;
        if (customHeader) {
            jsonResponseType = {
                ...jsonResponseType,
                ...customHeader
            };
        }
        if (!options.ignoreWrap) {
            if (result === undefined) {
                result = null;
            }
            result = JSON.stringify({response: result, status: "ok", code: 200});
        }
        resp.writeHead(200, jsonResponseType);
        resp.write(result);
        resp.end();
    }
};


var getImageSearchData = (params, context) => {
    var {searchKey} = params || {};
    var filter = {searchKey: {$exists: true}};
    if (searchKey) {
        filter["searchKey"] = searchKey;
    }

    var searchModel = getGoogleSearchModel();
    return new Promise((resolve, reject) => {
        searchModel.find(filter, 'searchKey files', function (err, result) {
            if (err) {
                reject(err);
            }
            resolve(result);
        });
    })
};


var getContentType = (fileName) => {
    var extension = fileName
        .split('.')
        .pop();
    var extensionTypes = {
        'css': 'text/css',
        'gif': 'image/gif',
        'jpg': 'image/jpeg',
        'jpeg': 'image/jpeg',
        'js': 'application/javascript',
        'png': 'image/png',
        'mp4': 'video/mp4',
        'mp3': 'audio/mpeg',
        'txt': 'text/plain',
        'pdf': 'application/pdf',
        'xls': 'application/vnd.openxmlformats',
        'xlsx': 'application/vnd.openxmlformats',
        'html': 'text/html',
        'htm': 'text/html'
    };
    if (!extension) {
        return;
    }
    extension = extension.toLowerCase();
    return extensionTypes[extension];
}

var uploadFilesOnServer = async (params, context) => {
    var searchKey = params.search && params.search.trim();
    if (!searchKey || searchKey.length === 0) {
        return;
    }
    var files = await googleImages(searchKey, context);
    var db = await getDB(context);
    if (!db) {
        throw new Error("DB Not found");
    }
    var filesData = await uploadFilesInMongo(files, db);

    var update = {
        searchKey,
        files: filesData
    }
    return findAndModify({searchKey}, update);
}

var findAndModify = (filter, update) => {
    var searchModel = getGoogleSearchModel();
    return new Promise((resolve, reject) => {
        searchModel.findOneAndUpdate(filter, {$set: update}, {upsert: true}, function (err, doc) {
            if (err) {
                reject(err);
            }
            resolve(doc);
        });
    })
};


var getRequestParams = (req) => {
    var allParams = {};
    var params = req.params || {};
    var body = req.body || {};
    var query = req.query || {};
    for (let key in params) {
        if (params.hasOwnProperty(key)) {
            allParams[key] = params[key];
        }
    }
    for (let key in body) {
        if (allParams[key] === void 0) {
            allParams[key] = body[key];
        }
    }
    for (let key in query) {
        if (allParams[key] === void 0) {
            allParams[key] = query[key];
        }
    }
    return allParams;
}

module.exports = {
    configure
};

