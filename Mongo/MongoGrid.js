import MongoDB from "./MongoDB"
import Utilities from "../Utilities";

var GridStore = require('mongodb').GridStore;
var objectID = require('mongodb').ObjectID;


var downloadFile = (fileKey, db) => {
    if (!fileKey) {
        throw new Error("fileKey not found ");
    }
    fileKey = convertToObjectId(fileKey);
    return new Promise((resolve, reject) => {
        var gridStore = new GridStore(db, fileKey, "r");
        gridStore.open(function (err) {
            if (err) {
                reject(err);
                return;
            }
            gridStore.seek(0, 0, function (err) {
                if (err) {
                    reject(err);
                    return;
                }
                gridStore.read(function (err, data) {
                    if (err) {
                        console.log("2121");
                        reject(err);
                        return;
                    }
                    resolve({fileName: gridStore.filename, contentType: gridStore.contentType, data: data});
                });
            });
        });
    })
};

var uploadFilesInMongo = (files, db) => {
    var isArray = true;
    if (!Array.isArray(files)) {
        files = [files];
        isArray = false;
    }
    var fileKeys = [];
    return Utilities.iterator(files, (index, file) => {
        file.fileKey = file.fileKey ? convertToObjectId(file.fileKey) : getUniqueId();
        return uploadFileInMongo(file.filename, file.fileKey, file.data, file.type, db).then(_ => {
            fileKeys.push({key: file.fileKey.toString(), name: file.filename});
        });
    }).then(_ => isArray ? fileKeys : fileKeys[0]);
}

var uploadFileInMongo = (fileName, fileKey, dataArray, content_type, db) => {
    return new Promise((resolve, reject) => {
        var gridStore = new GridStore(db, fileKey, fileName, "w", {content_type: content_type});
        gridStore.open(function (err) {
            if (err) {
                reject(err);
                return;
            }
            return Utilities.iterator(dataArray, (index, buffer) => {
                return new Promise((res, rej) => {
                    gridStore.write(buffer, function (err, result) {
                        if (err) {
                            rej(err);
                        } else {
                            res(result);
                        }
                    });
                });
            }).then(_ => {
                gridStore.close(function (err) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
            }).catch(err)
            {
                reject(err);
            }
        });
    })
}

var removeFile = (fileKeys, context) => {
    if (!Array.isArray(fileKeys)) {
        fileKeys = [fileKeys];
    }
    return MongoDB(context).then(db => {
        return Utilities.iterator(fileKeys, (index, fileKey) => {
            fileKey = convertToObjectId(fileKey);
            return new Promise((resolve, reject) => {
                var gridStore = new GridStore(db, fileKey, "r");
                gridStore.unlink(function (err, result) {
                    if (err) {
                        reject(err);
                    }
                    resolve();
                });
            })
        })
    })
}

function convertToObjectId(id) {
    if (typeof id === "string") {
        try {
            id = objectID(id);
        } catch (e) {
        }
    }
    return id;
}


function getUniqueId() {
    return new objectID;
}

module.exports = {
    uploadFilesInMongo,
    downloadFile
};
