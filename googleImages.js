import Utilities from "./Utilities"

const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');

const GoogleImages = require('google-images');
var Jimp = require("jimp");

var googleImages = async (searchKey, context) => {
    if (!searchKey) {
        return;
    }
    const client = new GoogleImages(context.SEARCH_ENGINE_ID, context.GOOGLE_PROJECT_API);
    var startTime = new Date();
    var results = [];
    var googleImages = await client.search(searchKey);
    if (!googleImages) {
        return results;
    }

    for (var i = 0; i < googleImages.length; i++) {
        try {
            var imageUrl = googleImages[i].url;
            var image = await  Jimp.read(imageUrl);
            if (!image) {
                continue;
            }
            var modifyImage = image.resize(240, 160).greyscale();
            var bufferData = await getBufferData(modifyImage, image);
            if (!bufferData) {
                continue;
            }

            var compressedData = await imagemin.buffer(bufferData, {
                plugins: [
                    imageminJpegtran(),
                    imageminPngquant({quality: '65-80'})
                ]
            })
            if (compressedData) {
                results.push({
                    filename: imageUrl.substring(imageUrl.lastIndexOf("/") + 1),
                    type: image.getMIME(),
                    data: [new Buffer(compressedData, "base64")]
                });
            }
        } catch (err) {
            console.log("err>>>>>>>>>>>>>.", err);
        }
    }

    return results;
};

var getBufferData = async (modifyImage, image) => {
    return new Promise((res, rej) => {
        modifyImage && modifyImage.getBuffer(image.getMIME(), (err, bufferData) => {
            if (err) {
                rej(err);
            }
            res(bufferData);
        });
    })
}

module.exports = {
    googleImages
};
