import context from "./context";
import {configure} from "./http";
import {initializeSearchModel} from "./schema";

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var port = process.env.PORT || 8080;


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.listen(port);
initializeSearchModel(context);

configure(app, context);


console.log('Magic happens at http://localhost:' + port);