
var iterator = (array, task) => {
    return new Promise((resolve, reject)=> {
        var length = array ? array.length : 0;
        if (length == 0) {
            resolve();
            return;
        }
        var index = 0;
        var loop = (index)=> {
            try {
                var onResolve = function () {
                    index = index + 1;
                    if (index == array.length) {
                        resolve();
                    } else {
                        loop(index);
                    }
                }
                try {
                    var p = task(index, array[index]);
                    if (!p) {
                        onResolve();
                        return;
                    }
                    p.then(onResolve)
                        .catch(function (err) {
                            reject(err)
                        })
                } catch (e) {
                    reject(e)
                }
            } catch (e) {
                reject(e)
            }
        }
        loop(index);
    })
}

module.exports = {
    iterator
};